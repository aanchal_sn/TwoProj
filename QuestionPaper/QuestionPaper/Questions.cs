﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionPaper
{
    public class Questions
    {
        public List<string> D = new List<string>();
        public List<string> A = new List<string>();
        public List<string> B = new List<string>();
        public List<string> C = new List<string>();
        public List<string> FinalList = new List<string>();

        int prevIndex, index, AIndex=0,BIndex=0,CIndex=0,DIndex=0, totalQues,AC=0,BC,CC,DC;

        public void CreateList()
        {
            for (int i = 0; i < 200; i++)
            {
                A.Add("a" + i);
            }
            for (int i = 0; i < 100; i++)
            {
                B.Add("b" + i);
            }
            for (int i = 0; i < 400; i++)
            {
                C.Add("c" + i);
            }
            for (int i = 0; i < 300; i++)
            {
                D.Add("d" + i);
            }
         

        }

        public int FindMax(int[] arr)
        {
            int max = 0;

            for (int i=0; i < 4;i++)
            {
                int temp = arr[i];
                if (temp > max)
                {
                    max = temp;
                    index = i;
                }
            }
            return index;
        }

        public void CreatePaper()
        {
            AC = A.Count();
            BC = B.Count();
            CC = C.Count();
            DC = D.Count();
            totalQues = AC + BC + CC + DC;
            int[] len = new int[4] { AC, BC, CC, DC };


            for (int loop=0;loop<totalQues;loop++)
            {
                int index = FindMax(len);
                if (index == prevIndex)
                {
                    index++;
                    if (index == 0)
                    {
                        FinalList.Add(A[AIndex]);

                        AIndex++;
                        //len[0]--;
                    }
                    else if (index == 1)
                    {
                        FinalList.Add(B[BIndex]);
                        BIndex++;
                       // len[1]--;

                    }
                    else if (index == 2)
                    {
                        FinalList.Add(C[CIndex]);
                        CIndex++;
                       // len[2]--;

                    }
                    else if (index == 3)
                    {
                        FinalList.Add(D[DIndex]);
                        DIndex++;
                       // len[3]--;

                    }
                    len[index]--;

                }
                else
                {
                    if (index == 0)
                    {
                        FinalList.Add(A[AIndex]);
                        AIndex++;
                        //len[0]--;
                    }
                    else if (index == 1)
                    {
                        //Console.WriteLine(B[BIndex]);
                        FinalList.Add(B[BIndex]);

                        BIndex++;
                       // len[1]--;

                    }
                    else if (index == 2)
                    {
                        //Console.WriteLine(C[CIndex]);
                        FinalList.Add(C[CIndex]);

                        CIndex++;
                       // len[2]--;

                    }
                    else if (index == 3)
                    {
                        //Console.WriteLine(D[DIndex]);
                        FinalList.Add(D[DIndex]);

                        DIndex++;
                       // len[3]--;

                    }
                    len[index]--;
                }
                
                prevIndex = index;
            }
        }

        public void DisplayQuestions()
        {
            for(int j=0;j<totalQues;j++)
            {
                Console.WriteLine(FinalList[j]);
            }
        }
    }
}
