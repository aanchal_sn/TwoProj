﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionPaper
{
    class Program
    {
        
         static void Main(string[] args)
        {
            Questions qs = new Questions();

            qs.CreateList();
            var watch = new System.Diagnostics.Stopwatch();
            watch.Start();

            qs.CreatePaper();
            watch.Stop();

            Console.WriteLine($"Execution Time: {watch.ElapsedMilliseconds} ms");
            qs.DisplayQuestions();

            Console.ReadKey();
        }

        

        
    }
}
