﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WordSuggestionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Words word = new Words();
            string ch="yes";
            while (ch != "no") 
            {
                Console.WriteLine("enter a word: ");
                string inputWord = Console.ReadLine();
                string searchWord = inputWord + "[\\w]*";
                bool flag = false;

                Regex regex = new Regex(searchWord, RegexOptions.IgnoreCase);
                var watch = new System.Diagnostics.Stopwatch();
                watch.Start();

                foreach (string str in word.wordList)
                {
                    Match match = regex.Match(str);
                    if (match.Success)
                    {
                        flag = true;
                        Console.WriteLine("suggested word: " + str);
                    }


                }
                if (flag == false)
                {

                    Console.WriteLine  ("no match found");
                    word.wordList.Add(inputWord);
                    
                }
                watch.Stop();
                Console.WriteLine($"Execution Time: {watch.ElapsedMilliseconds} ms");

                Console.WriteLine("want to try again? (yes/no)");
                ch=Console.ReadLine();
            } 


            Console.ReadKey();
        }
    }
}
